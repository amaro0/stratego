const $aivsai = $("#aivsai");
const $pvsai = $("#pvsai");
const $pvsp = $("#pvsp");
const $start = $("#start");
const $boardSizeInput = $('#board-size-input');
const $board = $("#board");
const $player2Points = $("#player2-points");
const $player1Points = $("#player1-points");
const $alfabeta1 = $("#alfabeta1");
const $minmax1 = $("#minmax1");
const $alfabeta2 = $("#alfabeta2");
const $minmax2 = $("#minmax2");
const $depthP2Input = $("#depth-p2-input");
const $depthP1Input = $("#depth-p1-input");


let boardSize = 5;
let depthP1 = 3;
let depthP2 = 3;
let playerTurn =1;
let player1Color="black";
let player2Color="blue";
let player1Points=0;
let player2Points=0;
let gameMode ='pvsp';

let boardArr =createArray(boardSize);
let socket;

$(function() {
    socket = io.connect('http://' + document.domain + ':' + location.port,{
        'reconnection': true,
        'reconnectionDelay': 1,
        'reconnectionDelayMax' : 1,
        'reconnectionAttempts': Infinity
    });
    updateBoardSize();
    $pvsp.button('toggle');
    $alfabeta2.button('toggle');
    $alfabeta1.button('toggle');
    let active = $pvsp;
    let algorithm1 = "alfabeta";
    let algorithm2 = "alfabeta";
    $player1Points.text(player1Points);
    $player2Points.text(player2Points);

    $pvsp.click(function () {
        $pvsp.button('toggle');
        active.button('toggle');
        active = $pvsp;
        gameMode = 'pvsp';
    });
    $pvsai.click(function () {
        $pvsai.button('toggle');
        active.button('toggle');
        active = $pvsai;
        gameMode = 'pvsai';
    });
    $aivsai.click(function () {
        $aivsai.button('toggle');
        active.button('toggle');
        active = $aivsai;
        gameMode = 'aivsai';
    });
    $alfabeta1.click(function () {
        $minmax1.button('toggle');
        $alfabeta1.button('toggle');
        algorithm1='alfabeta';
    });
    $minmax1.click(function () {
        $minmax1.button('toggle');
        $alfabeta1.button('toggle');
        algorithm1='minmax';
    });
    $alfabeta2.click(function () {
        $minmax2.button('toggle');
        $alfabeta2.button('toggle');
        algorithm2='alfabeta';
    });
    $minmax2.click(function () {
        $minmax2.button('toggle');
        $alfabeta2.button('toggle');
        algorithm2='minmax';
    });
    $start.click(function () {
        updateBoardSize();
        boardArr = createArray(boardSize);
        playerTurn = 1;
        player1Points = 0;
        player2Points = 0;
        $player1Points.text(player1Points);
        $player2Points.text(player2Points);
        socket.emit('start game',gameMode, algorithm1,algorithm2);
    });
    $board.click(function (event) {
        let target = $(event.target);
        let id = target[0].id;
        let xy=getXYFromId(id);
        if(boardArr[xy.x][xy.y]==undefined) {
            if (playerTurn == 1) {
                $("#" + id).css("background", player1Color);
                socket.emit('move',playerTurn,xy.x,xy.y)
                playerTurn = 2;
                boardArr[xy.x][xy.y] = playerTurn;
            } else {
                $("#" + id).css("background", player2Color);
                socket.emit('move',playerTurn,xy.x,xy.y)
                playerTurn = 1;
                boardArr[xy.x][xy.y] = playerTurn;
            }
        }
    });

    $boardSizeInput.val(boardSize);
    $boardSizeInput.change(function () {
        boardSize = $boardSizeInput.val();
        updateBoardSize();
        boardArr = createArray(boardSize)

    });
    $depthP1Input.val(depthP1);
    $depthP1Input.change(function () {
        depthP1 = $depthP1Input.val();
        updatePlayerDepth(1,depthP1)
    });
    $depthP2Input.val(depthP2);
    $depthP2Input.change(function () {
        depthP2 = $depthP2Input.val();
        updatePlayerDepth(2,depthP2)
    });

    socket.on('connect', function() {
        console.log('socket connected');
        socket.emit('my event', {data: 'I\'m connected!'});
    });

    socket.on("move result", function(player1_points,player2_points) {
        $player1Points.text(player1_points);
        $player2Points.text(player2_points);
    });

    socket.on('winner', function(winner,player1_points,player2_points) {
        $player1Points.text(player1_points);
        $player2Points.text(player2_points);
        alert(winner+' won!!!');
    });

    socket.on('draw', function(player1_points,player2_points) {
        $player1Points.text(player1_points);
        $player2Points.text(player2_points);
        alert('draw :(');
    });

    socket.on("move result ai", function(player1_points,player2_points, x, y) {
        console.log('ai move')
        let boardElemIdString='#x'+x+'y'+y;
        $player1Points.text(player1_points);
        $player2Points.text(player2_points);
        if (playerTurn == 1) {
                $(boardElemIdString).css("background", player1Color);
                playerTurn = 2;
                boardArr[x][y] = playerTurn;
        } else {
            $(boardElemIdString).css("background", player2Color);
            playerTurn = 1;
            boardArr[x][y] = playerTurn;
        }
    });
});


function updateBoardSize(){
    $.get('/boardsizeupdate',{size:boardSize},function (result) {
        let html = $(result);
        $board.html(html);
    });
}


function updatePlayerDepth(player,depth){
    $.get('/playerdepthupdate',{player:player, depth:depth});
}

function createArray(length) {
    length = parseInt(length);
    let arr = new Array(length);
    for (let i = 0; i < 10; i++) {
      arr[i] = new Array(length);
    }
    return arr;
}

function getXYFromId(id){
    let x = parseInt(id.charAt(1));
    let y = parseInt(id.charAt(3));
    return {x:x, y:y};
}