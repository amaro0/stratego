from Game import Game
#
# game = Game(7)
# game.check_if_points(1,1)
from flask import Flask
from flask import render_template
from flask import request
from flask_socketio import SocketIO, emit
from flask import url_for
from jinja2 import Environment, PackageLoader, select_autoescape


app = Flask(__name__)
socketio = SocketIO(app)
if __name__ == '__main__':
    socketio.run(app)

# set FLASK_APP=main.py
# set FLASK_DEBUG=1
#
# url_for('static', filename='style.css')
# url_for('static', filename='script.js')
board_size = 5
depthP1 = 3
depthP2 = 3


@app.route('/')
def hello_world():
    return render_template('layout.html', size=board_size)

@app.route('/boardsizeupdate', methods=['GET'])
def update_board_size():
    global board_size
    board_size = int(request.args['size'])
    return render_template('board.html', size=board_size)

@app.route('/playerdepthupdate', methods=['GET'])
def update_player_depth_size():
    global depthP1, depthP2
    player = int(request.args['player'])
    if player == 1:
        depthP1 = int(request.args['depth'])
        print('zmiana depth',depthP1)
    else:
        depthP2 = int(request.args['depth'])
        print('zmiana depth', depthP2)
    return ('', 204)

@socketio.on('connect')
def test_connect():
    print('socket connected')

def game_pvsp(algorithm1, algorithm2):
    print('start game pvsp')
    game = Game(board_size)

    @socketio.on('move')
    def move(player,x,y):
        game_status, winner, player1_points, player2_points = game.move_user(player, x, y)
        print(game_status, winner, player1_points, player2_points)
        if game_status=="ongoing":
            emit("move result", (player1_points,player2_points))
        else:
            if game_status=="winner":
                emit("winner", (winner,player1_points,player2_points))
            else:
                emit("draw", (winner, player1_points, player2_points))

def game_pvsai(algorithm1, algorithm2):
    print('start game pvsai')
    game = Game(board_size)

    @socketio.on('move')
    def move(player, x, y):
        game_status, winner, player1_points, player2_points = game.move_user(player, x, y)
        print(game_status, winner, player1_points, player2_points)
        if game_status == "ongoing":
            print('movedep',depthP2)
            game_status, winner, player1_points, player2_points, pos = game.move_ai(2,algorithm2,depthP2)
            if game_status == "ongoing":
                emit("move result ai", (player1_points, player2_points, pos[0], pos[1]))
            else:
                if game_status == "winner":
                    emit("winner", (winner, player1_points, player2_points))
                else:
                    emit("draw", (winner, player1_points, player2_points))
        else:
            if game_status == "winner":
                emit("winner", (winner, player1_points, player2_points))
            else:
                emit("draw", (winner, player1_points, player2_points))
        print('-------------------------end move-----------------------------')


def game_aivsai(algorithm1,algorithm2):
    print('start game aivsai')
    end_of_game = False
    player = 1
    game = Game(board_size)
    while not end_of_game:
        if player == 1:
            print('p1',depthP1)
            game_status, winner, player1_points, player2_points, pos = game.move_ai(player, algorithm1, depthP1)
            print(game_status, winner, player1_points, player2_points, pos)
            player = 2
        else:
            print('p2',depthP1)
            game_status, winner, player1_points, player2_points, pos = game.move_ai(player, algorithm2, depthP2)
            print(game_status, winner, player1_points, player2_points, pos)
            player = 1
        if game_status == "ongoing":
            emit("move result ai", (player1_points, player2_points, pos[0], pos[1]))
            # socketio.sleep(0.3)
        else:
            if game_status == "winner":
                emit("move result ai", (player1_points, player2_points, pos[0], pos[1]))
                emit("winner", (winner, player1_points, player2_points))
            else:
                emit("move result ai", (player1_points, player2_points, pos[0], pos[1]))
                emit("draw", (winner, player1_points, player2_points))
            end_of_game = True
        print('-------------------------end move-----------------------------')

game_mode_switcher = {
    "pvsp": game_pvsp,
    "pvsai": game_pvsai,
    "aivsai": game_aivsai
}

@socketio.on('start game')
def start_game(mode,algorithm1,algorithm2):
    func = game_mode_switcher.get(mode, "none")
    return func(algorithm1,algorithm2)