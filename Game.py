import numpy as np
import copy
import sys
from operator import itemgetter

class Game:

    def __init__(self, size):
        print('new game size: ',size)
        self.size = size
        self.player1_points = 0
        self.player2_points = 0
        self.board = np.zeros((size,size))
        self.end_game_counter = 0
        self.end_game_counter_finish = size*size
        self.best_move =None
        self.algoritmh1_depth=None
        self.algoritmh2_depth=None

    def move_user(self, player, row, col):
        self.board[row][col] = player
        points = self.calculate_points(row, col, self.board)
        if player == 1:
            self.player1_points += points
        else:
            self.player2_points += points
        self.end_game_counter += 1
        if self.end_game_counter == self.end_game_counter_finish:
            if self.player1_points > self.player2_points:
                return 'winner', 'player1', self.player1_points, self.player2_points
            elif self.player1_points < self.player2_points:
                return 'winner', 'player2', self.player1_points, self.player2_points
            else:
                return 'draw', 'none', self.player2_points, self.player2_points
        return "ongoing", "none", self.player1_points, self.player2_points

    def move_ai(self, player, algorithm, depth):
        if algorithm == 'minmax':
            return self.move_minmax(player, depth)
        else:
            return self.move_alfabeta(player, depth)

    def move_alfabeta(self, player, depth):
        possible_moves = self.create_list_of_free_places()
        alfa = float('-inf'), (0, 0)
        beta = float('inf'), None
        points, pos = self.alfabeta(depth, True, possible_moves, self.board, (0, 0), alfa, beta, depth, 0, 0)
        print(pos)

        # self.board[pos[0]][pos[1]] = player
        self.board[self.best_move[0]][self.best_move[1]] = player
        points = self.calculate_points(self.best_move[0], self.best_move[1], self.board)
        if player == 1:
            self.player1_points += points
        else:
            self.player2_points += points
        self.end_game_counter += 1
        print(self.end_game_counter)
        if self.end_game_counter == self.end_game_counter_finish:
            if self.player1_points > self.player2_points:
                return 'winner', 'player1', self.player1_points, self.player2_points, self.best_move
            elif self.player1_points < self.player2_points:
                return 'winner', 'player2', self.player1_points, self.player2_points, self.best_move
            else:
                return 'draw', 'none', self.player2_points, self.player2_points, self.best_move
        return "ongoing", "none", self.player1_points, self.player2_points, self.best_move

    def move_minmax(self,player, depth):
        possible_moves = self.create_list_of_free_places()
        points, pos = self.minmax(depth, True, possible_moves, self.board, (0, 0), depth, 0, 0)
        print(pos)
        # self.board[pos[0]][pos[1]] = player
        self.board[self.best_move[0]][self.best_move[1]] = player
        # points = self.calculate_points(pos[0], pos[1], self.board)
        points = self.calculate_points(self.best_move[0], self.best_move[1], self.board)
        if player == 1:
            self.player1_points += points
        else:
            self.player2_points += points
        self.end_game_counter += 1
        print(self.end_game_counter)
        if self.end_game_counter == self.end_game_counter_finish:
            if self.player1_points > self.player2_points:
                return 'winner', 'player1', self.player1_points, self.player2_points, self.best_move
            elif self.player1_points < self.player2_points:
                return 'winner', 'player2', self.player1_points, self.player2_points, self.best_move
            else:
                return 'draw', 'none', self.player2_points, self.player2_points, self.best_move
        return "ongoing", "none", self.player1_points, self.player2_points, self.best_move

    def minmax(self, depth, isMax, possible_moves, board, node, top_level, points_p1, points_p2):
        if depth == 0 or len(possible_moves) == 0:
            return points_p1-points_p2, node
        if isMax:
            best = -1* sys.maxsize, (0, 0)
            for i in range(0, len(possible_moves)):
                curr_possible_moves = copy.deepcopy(possible_moves)
                curr_pos = curr_possible_moves.pop(i)
                curr_board = copy.deepcopy(board)
                curr_board[curr_pos[0]][curr_pos[1]] = 1
                curr_p1 = copy.deepcopy(points_p1)
                curr_p2 = copy.deepcopy(points_p2)
                curr_p1 += self.calculate_points(curr_pos[0], curr_pos[1], curr_board)
                res = self.minmax(depth-1, False, curr_possible_moves,  curr_board, curr_pos, top_level, curr_p1, curr_p2)
                print('res',res)
                # best = max([best, res], key=itemgetter(0))
                if best[0] < res[0]:
                    best=res
                    if depth == top_level:
                        self.best_move = curr_pos
                        print('best move',self.best_move)
            return best
        else:
            best = sys.maxsize, None
            for i in range(0, len(possible_moves)):
                curr_possible_moves = copy.deepcopy(possible_moves)
                curr_pos = curr_possible_moves.pop(i)
                curr_board = copy.deepcopy(board)
                curr_board[curr_pos[0]][curr_pos[1]] = 2
                curr_p1 = copy.deepcopy(points_p1)
                curr_p2 = copy.deepcopy(points_p2)
                curr_p2 += self.calculate_points(curr_pos[0], curr_pos[1], curr_board)
                res = self.minmax(depth - 1, True, curr_possible_moves, curr_board, curr_pos, top_level, curr_p1, curr_p2)
                # best = min([best, res], key=itemgetter(0))
                if best[0] > res[0]:
                    best=res
            return best

    def alfabeta(self, depth, isMax, possible_moves, board, node, alfa, beta, top_level, points_p1, points_p2):
        if depth == 0 or len(possible_moves) == 0:
            # return self.calculate_points(node[0],node[1],board), node
            return points_p1 - points_p2, node
        if isMax:
            for i in range(0, len(possible_moves)):
                curr_possible_moves = copy.deepcopy(possible_moves)
                curr_pos = curr_possible_moves.pop(i)
                curr_board = copy.deepcopy(board)
                curr_board[curr_pos[0]][curr_pos[1]] = 1
                curr_p1 = copy.deepcopy(points_p1)
                curr_p2 = copy.deepcopy(points_p2)
                curr_p1 += self.calculate_points(curr_pos[0], curr_pos[1], curr_board)
                res = self.alfabeta(depth - 1, False, curr_possible_moves, curr_board, curr_pos, alfa, beta, top_level, curr_p1, curr_p2)
                # print('res', res)
                # best = max([best, res], key=itemgetter(0))
                if res[0] > alfa[0]:
                    alfa = res
                    if depth == top_level:
                        self.best_move = curr_pos
                        print('best_move',self.best_move)
                if alfa[0] >= beta[0]:
                    break
                curr_board[curr_pos[0]][curr_pos[1]] = 0
            return alfa
        else:
            for i in range(0, len(possible_moves)):
                curr_possible_moves = copy.deepcopy(possible_moves)
                curr_pos = curr_possible_moves.pop(i)
                curr_board = copy.deepcopy(board)
                curr_board[curr_pos[0]][curr_pos[1]] = 2
                curr_p1 = copy.deepcopy(points_p1)
                curr_p2 = copy.deepcopy(points_p2)
                curr_p2 += self.calculate_points(curr_pos[0], curr_pos[1], curr_board)
                res = self.alfabeta(depth - 1, True, curr_possible_moves, curr_board, curr_pos, alfa, beta, top_level, curr_p1, curr_p2)
                # best = min([best, res], key=itemgetter(0))
                if res[0] < beta[0]:
                    beta = res
                if alfa[0] >= beta[0]:
                    break
            return beta

    def create_list_of_free_places(self):
        result_list = []
        for i in range(self.size):
            for j in range(self.size):
                if self.board[i][j] == 0:
                    result_list.append((i,j))
        return result_list

    def calculate_points(self, row, col, board):
        temp_points = 0
        row_arr = board[row,:]
        col_arr = board[:,col]
        major_diag_arr = np.diagonal(board, offset=(col - row))
        minor_diag_arr = np.diagonal(np.rot90(board), offset=-board.shape[1] + (col + row) + 1)
        if 0 not in row_arr:
            temp_points += len(row_arr)
        if 0 not in col_arr:
            temp_points += len(col_arr)
        if len(major_diag_arr) > 1:
            if 0 not in major_diag_arr:
                temp_points += len(major_diag_arr)
        if len(minor_diag_arr) > 1:
            if 0 not in minor_diag_arr:
                temp_points += len(minor_diag_arr)
        return temp_points

